from unittest import TestCase

from Calculadora import Calc

class CalcTest(TestCase):
    def test_calculate(self):
        self.assertEqual(Calc().calculate("")[0],0,"Cadena Vacia")
        self.assertEqual(Calc().calculate("2")[0], 1, "Cadena un numero")
        self.assertEqual(Calc().calculate("2,5")[0], 2, "Dos numeros")
        self.assertEqual(Calc().calculate("2,5,6,3")[0], 4, "Varios numeros")

    def test_calculateMin(self):
        self.assertEqual(Calc().calculate("")[1],0,"Cadena Vacia")
        self.assertEqual(Calc().calculate("4")[1], 4, "Cadena un numero")
        self.assertEqual(Calc().calculate("3,7")[1], 3, "Dos numeros")
        self.assertEqual(Calc().calculate("5,1,8,2,7,9")[1], 1, "Varios numeros")

    def test_calculateMax(self):
        self.assertEqual(Calc().calculate("")[2],0,"Cadena Vacia")
        self.assertEqual(Calc().calculate("4")[2], 4, "Cadena un numero")
        self.assertEqual(Calc().calculate("3,7")[2], 7, "Dos numeros")
        self.assertEqual(Calc().calculate("5,1,8,2,7,9")[2], 9, "Varios numeros")

    def test_calculatePromedio(self):
        self.assertEqual(Calc().calculate("")[3], 0, "Cadena Vacia")
        self.assertEqual(Calc().calculate("4")[3], 4, "Cadena un numero")
        self.assertEqual(Calc().calculate("3,7")[3], 5, "Dos numeros")
        self.assertEqual(Calc().calculate("5,1,8,2,7,9")[2], 9, "Varios numeros")

