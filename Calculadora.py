__author__ = 'Camilo forero y Jairo Solarte'


class Calc:
    def calculate(self, cadena):
        estadisticas = []
        if(cadena == ""):
            estadisticas = [0] * 4;
        else:
            estad = cadena.split(',')
            estad.sort()
            estadisticas.append(len(estad))
            estadisticas.append(int(min(estad)))
            estadisticas.append(int(max(estad)))
            estadisticas.append(self.promediarLista(estad))
        return estadisticas

    def promediarLista(self,lista):
        sum = 0.0
        for i in range(0, len(lista)):
            sum = sum + float(lista[i])

        return sum / len(lista)